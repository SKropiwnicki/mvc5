﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApplication1
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

                        
            routes.MapRoute(
                name: "Role",
                url: "Role",
                defaults: new { controller = "Role", action = "Index" }
            );
            routes.MapRoute(
                name: "Weapon",
                url: "Weapons",
                defaults: new { controller = "Weapons", action = "Index" }
            );
            routes.MapRoute(
                name: "Fighter",
                url: "Fighters",
                defaults: new { controller = "Fighters", action = "Index"}
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                );
        }
    }
}
