﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Projekt1.Models;

namespace WebApplication1.Models
{
    public class Fighter
    {
        public int FighterID { get; set; }

        [Required(ErrorMessage = "Wymagane pierwsze imie", AllowEmptyStrings = false)]
        [MyValidator]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Nazwisko wymagane", AllowEmptyStrings = false)]
        [MyValidator]
        public string LastName { get; set; }

        public string Class { get; set; }

        public int WeaponID { get; set; }


        public DateTime Birthday { get; set; }
        
        public virtual Weapon Weapon { get; set; }
       
    }
}