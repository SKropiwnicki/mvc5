﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Projekt1.Models
{
    public class MyValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                string weaponName = value.ToString();

                if (Regex.IsMatch(weaponName, @"^[A-z]+$", RegexOptions.IgnoreCase))
                {
                    return ValidationResult.Success;
                }
                else
                {
                    return new ValidationResult("Wprowadź poprawną nazwę broni! Same literki!");
                }
            }
            else
            {
                return new ValidationResult("Pole" + validationContext.DisplayName + " wymagane.");
            }
        }
    }
}