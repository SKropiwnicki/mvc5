﻿using Projekt1.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace WebApplication1.Models
{
    public class Weapon
    {
        public int ID { get; set; }


        [Required(ErrorMessage = "Nazwa jest wymagana", AllowEmptyStrings = false)]
        [MyValidator]
        public string WeaponName{ get; set; }

        [Required(ErrorMessage = "Bron potrzebuje obrazen", AllowEmptyStrings = false)]
        public int Damage { get; set; }

        [Required(ErrorMessage = "Wprowadz Speed broni", AllowEmptyStrings = false)]
        public float Speed { get; set; }

        public string Description { get; set; }
        
        
        

        // kolekcja wojownikow uzywajacych tej broni.
        public virtual ICollection<Fighter> Fighters { get; set; }


    }


}