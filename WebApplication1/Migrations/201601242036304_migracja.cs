namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migracja : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Fighters", "FirstName", c => c.String(nullable: false));
            AlterColumn("dbo.Fighters", "LastName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Fighters", "LastName", c => c.String());
            AlterColumn("dbo.Fighters", "FirstName", c => c.String());
        }
    }
}
