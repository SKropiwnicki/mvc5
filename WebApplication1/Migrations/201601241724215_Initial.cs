namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Fighters",
                c => new
                    {
                        FighterID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Class = c.String(),
                        WeaponID = c.Int(nullable: false),
                        Birthday = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.FighterID)
                .ForeignKey("dbo.Weapons", t => t.WeaponID, cascadeDelete: true)
                .Index(t => t.WeaponID);
            
            CreateTable(
                "dbo.Weapons",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        WeaponName = c.String(nullable: false),
                        Damage = c.Int(nullable: false),
                        Speed = c.Single(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Fighters", "WeaponID", "dbo.Weapons");
            DropIndex("dbo.Fighters", new[] { "WeaponID" });
            DropTable("dbo.Weapons");
            DropTable("dbo.Fighters");
        }
    }
}
