﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class FightersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Fighters
        [Authorize(Roles = "God, Manager")]
        public ActionResult Index(string searchString)
        {


            var fighter = db.Fighter.Include(f => f.Weapon);

            if (!String.IsNullOrEmpty(searchString))
            {
                fighter = fighter.Where(s => s.FirstName.Contains(searchString));
            }

            return View(fighter);
        }

        // GET: Fighters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Fighter fighter = db.Fighter.Find(id);
            if (fighter == null)
            {
                return HttpNotFound();
            }
            return View(fighter);
        }

        // GET: Fighters/Create
        public ActionResult Create()
        {
            ViewBag.WeaponID = new SelectList(db.Weapon, "ID", "WeaponName");
            return View();
        }

        // POST: Fighters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FighterID,WeaponID,FirstName,LastName,Class,Birthday")] Fighter fighter)
        {
            if (ModelState.IsValid)
            {
                db.Fighter.Add(fighter);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.WeaponID = new SelectList(db.Weapon, "ID", "WeaponName", fighter.WeaponID);
            return View(fighter);
        }

        // GET: Fighters/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Fighter fighter = db.Fighter.Find(id);
            if (fighter == null)
            {
                return HttpNotFound();
            }
            ViewBag.WeaponID = new SelectList(db.Weapon, "ID", "WeaponName", fighter.WeaponID);
            return View(fighter);
        }

        // POST: Fighters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FighterID,WeaponID,FirstName,LastName,Class,Birthday")] Fighter fighter)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fighter).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.WeaponID = new SelectList(db.Weapon, "ID", "WeaponName", fighter.WeaponID);
            return View(fighter);
        }

        // GET: Fighters/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Fighter fighter = db.Fighter.Find(id);
            if (fighter == null)
            {
                return HttpNotFound();
            }
            return View(fighter);
        }

        // POST: Fighters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Fighter fighter = db.Fighter.Find(id);
            db.Fighter.Remove(fighter);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
